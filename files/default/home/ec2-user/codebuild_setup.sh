#!/bin/bash

dockerBuildAwsCodeBuildAl2() {
  docker inspect --type=image aws/codebuild/al2:3.0 > /dev/null 2>&1 && return 0
  local tempdir=`mktemp -d`
  pushd $tempdir > /dev/null
  git clone https://github.com/aws/aws-codebuild-docker-images.git .
  cd al2/x86_64/standard/3.0
  sed -i '/^ENTRYPOINT/d' Dockerfile
  docker build -t aws/codebuild/al2:3.0 .
  popd > /dev/null
  rm -rf $tempdir
}

dockerBuildAwsCodeBuildAl2
