package "openssh-server" do
  action :install
end

execute 'reconfigure ssh' do
  command 'dpkg-reconfigure openssh-server'
  not_if { ::File.readlines('/etc/ssh/sshd_config').grep(/AllowUsers ec2-user/).size > 0 }
end

cookbook_file '/etc/ssh/sshd_config' do
  source 'etc/ssh/sshd_config'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
  notifies :run, 'execute[restart ssh]', :immediately
end

execute 'restart ssh' do
  command 'service ssh --full-restart'
  action :nothing
end

execute 'ssh-keygen' do
  command "ssh-keygen -t rsa -N '' -C 'workstation' -f /home/ec2-user/.ssh/id_rsa"
  not_if { ::File.exist?('/home/ec2-user/.ssh/id_rsa') }
end

file '/home/ec2-user/.ssh/id_rsa' do
  mode '0600'
  owner 'ec2-user'
  group 'ec2-user'
end

file '/home/ec2-user/.ssh/id_rsa.pub' do
  mode '0600'
  owner 'ec2-user'
  group 'ec2-user'
end

file '/home/ec2-user/.ssh/authorized_keys' do
  mode '0600'
  owner 'ec2-user'
  group 'ec2-user'
end

execute 'cat id_rsa.pub to authorized_keys' do
  command "cat /home/ec2-user/.ssh/id_rsa.pub >> /home/ec2-user/.ssh/authorized_keys"
  not_if { ::File.readlines('/home/ec2-user/.ssh/authorized_keys').grep(/workstation/).size > 0 }
end
