cookbook_file '/usr/local/bin/awscb' do
  source 'usr/local/bin/awscb'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

remote_file '/usr/local/bin/codebuild_build.sh' do
  source 'https://raw.githubusercontent.com/aws/aws-codebuild-docker-images/master/local_builds/codebuild_build.sh'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

cookbook_file '/home/ec2-user/codebuild_setup.sh' do
  source 'home/ec2-user/codebuild_setup.sh'
  owner 'ec2-user'
  group 'ec2-user'
  mode '0755'
  action :create
end
