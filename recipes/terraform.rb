remote_file '/tmp/terraform.zip' do
  source "https://releases.hashicorp.com/terraform/0.12.9/terraform_0.12.9_linux_amd64.zip"
  retries 5
  retry_delay 5
  action :create
  not_if { ::File.exist?('/usr/local/bin/terraform') }
end

execute 'unzip terraform' do
  command 'unzip /tmp/terraform.zip'
  cwd '/usr/local/bin/'
  not_if { ::File.exist?('/usr/local/bin/terraform') }
end

file '/tmp/terraform.zip' do
  action :delete
end

