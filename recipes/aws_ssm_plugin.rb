remote_file '/tmp/session-manager-plugin.deb' do
  source 'https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  not_if { ::File.exist?('/usr/local/bin/session-manager-plugin') }
end

execute 'install session-manager-plugin' do
  command 'dpkg -i /tmp/session-manager-plugin.deb'
  not_if { ::File.exist?('/usr/local/bin/session-manager-plugin') }
end

file '/tmp/session-manager-plugin.deb' do
  action :delete
end
