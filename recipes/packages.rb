execute 'apt-get update' do
  command 'apt-get update'
end

package "sudo" do
  action :install
end

package "git" do
  action :install
end

package "wget" do
  action :install
end

package "unzip" do
  action :install
end

package "python3" do
  action :install
end

package "python3-pip" do
  action :install
end

package "tree" do
  action :install
end

package "jq" do
  action :install
end

package "ruby" do
  action :install
end

execute 'pip3 install awscli' do
  command 'pip3 install awscli'
  not_if { `pip3 show awscli`.match(/awscli/) }
end

execute 'pip3 install boto3' do
  command 'pip3 install boto3'
  not_if { `pip3 show boto3`.match(/boto3/) }
end

execute 'pip3 install git-remote-codecommit' do
  command 'pip3 install git-remote-codecommit'
  not_if { `pip3 show git-remote-codecommit`.match(/git-remote-codecommit/) }
end
