describe group('ec2-user') do
  it { should exist }
  its('gid') { should eq 1000 }
end

describe user('ec2-user') do
  it { should exist }
  its('uid') { should eq 1000 }
  its('gid') { should eq 1000 }
  its('groups') { should eq ['ec2-user'] }
end
